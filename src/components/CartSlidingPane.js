import React, { useState, useEffect } from 'react';
import SlidingPane from 'react-sliding-pane';
import Swal from 'sweetalert2';
import 'react-sliding-pane/dist/react-sliding-pane.css';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';

export default function CartSlidingPane({ isOpen, onRequestClose }) {
  const [cartContent, setCartContent] = useState([]);
  const [error, setError] = useState(null);

  const fetchCarts = async () => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_URL}/users/allCarts`, {
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${localStorage.getItem('token')}`,
        },
      });

      if (res.ok) {
        const data = await res.json();
        setCartContent(data);
      } else {
        throw new Error("Failed to fetch carts");
      }
    } catch (error) {
      setError({ message: 'Error: Failed to fetch cart content' });
    }
  };

  const placeOrder = async (productId) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({ productId: productId })
      });

      if (res) {
        const data = await res.json();
        // Check the response to determine if the order was placed successfully
        if (data) {
          // Order placed successfully
          Swal.fire({
            icon: 'success',
            title: 'Order Placed',
            text: 'Your order has been placed successfully.',
          });
          removeCart(productId);
          fetchCarts(); // Fetch the updated data after placing the order
        } else {
          // Order placement failed
          Swal.fire({
            icon: 'error',
            title: 'Order Failed',
            text: 'Failed to place your order.',
          });
        }
      } else {
        throw new Error("Failed to place order. Data invalid");
      }
    } catch (error) {
      console.error(error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Failed to place your order. Please try again.',
      });
    }
  };

  const removeCart = async (productId) => {
    try {
      const res = await fetch(`${process.env.REACT_APP_API_URL}/users/remove`, {
        method: 'PUT',
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({ productId: productId })
      });

      if (res) {
        const data = await res.json();
        console.log(data);
        // Check the response to determine if the cart item was removed successfully
        if (data) {
          // Cart item removed successfully
          // Swal.fire({
          //   icon: 'success',
          //   title: 'Item Removed',
          //   text: 'The item has been removed from your cart.',
          // });
          fetchCarts(); // Fetch the updated data after removing the cart item
        } else {
          // Cart item removal failed
          Swal.fire({
            icon: 'error',
            title: 'Removal Failed',
            text: 'Failed to remove the item from your cart.',
          });
        }
      } else {
        throw new Error("Failed to remove cart item");
      }
    } catch (error) {
      console.error(error);
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Failed to remove the item from your cart. Please try again.',
      });
    }
  };

  useEffect(() => {
    if (isOpen) {
      fetchCarts();
    }
  }, [isOpen]);

  return (
    <SlidingPane
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      width="300px"
      className="sliding-pane"
      overlayClassName="sliding-pane-overlay"
    >
      <h2>Cart</h2>
      {/* Display the fetched cart content */}
      {Array.isArray(cartContent) ? (
        cartContent.map((item) => (
          <div className="row product" key={item._id}>
            <div className="col-md-8 product-detail">
              <p>Product Name: {item.productName}</p>
              <p>Added On: {item.addedOn}</p>
              <p>Status: {item.status}</p>
              <button className="order-btn" onClick={() => placeOrder(item.productId)}><AddOutlinedIcon /></button>
              <button className="order-btn" onClick={() => removeCart(item.productId)}><ClearOutlinedIcon /></button>
            </div>
          </div>
        ))
      ) : (
        <p>No cart content available.</p>
      )}
    </SlidingPane>
  );
}
import React from 'react';

const Background = () => {
  return (
    <div className="background">
      <img src={process.env.PUBLIC_URL + '/images/background5.jpg'} alt="Background5" className="background-image" />
    </div>
  );
};

export default Background;
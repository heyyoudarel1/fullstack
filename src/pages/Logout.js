import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom'; // Explicitly import Navigate
import UserContext from '../UserContext';

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);

  // Clear the localStorage
  unsetUser();

  useEffect(() => {
    setUser({ id: null });
  }, [setUser]); // Add setUser as a dependency

  // Redirect back to login
  return <Navigate to="/" />;
}

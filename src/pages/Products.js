import React from 'react';
import AppNavbar from '../components/AppNavbar';
import ProductBackground from '../components/background/ProductBackground';
import ProductRow from '../components/ProductRow';

const ProductList = () => {
  return (
    <div>
      <AppNavbar />
      <ProductBackground />
      <div className="container main-content">
        <ProductRow />
      </div>
    </div>
  );
};

export default ProductList;
import React, { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import AppNavbar from '../components/AppNavbar';
import { Table } from "react-bootstrap";
import { Navigate } from "react-router-dom";


export default function MyOrders() {
  const { user } = useContext(UserContext);
  const [myOrders, setMyOrders] = useState([]);

  const fetchOrders = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      if (response.ok) {
        const data = await response.json();
        setMyOrders(data);
      } else {
        throw new Error("Failed to fetch orders");
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchOrders();
  }, []);

  return (
    !user.isAdmin ? (
      <>
        <AppNavbar />
        <div className="background">
          <img src={process.env.PUBLIC_URL + '/images/background4.jpg'} alt="Background1" className="background-login" />
        </div>
        {/* Header for the admin dashboard and functionality for create product and show order */}
        <div className="myorder mt-5 mb-3 text-center">
          <h1>My Orders</h1>
        </div>

        <div className="tableContainer">
          <Table className="orderTable col-12" striped bordered hover>
            <thead>
              <tr>
                <th><h3>Product Name</h3></th>
                <th><h3>Date and Time</h3></th>
                <th><h3>Status</h3></th>
              </tr>
            </thead>
            <tbody>
              {myOrders.map((order, index) => (
                <tr key={`${order.productId}-${index}`}>
                  <td>{order.productName}</td>
                  <td>{order.ordersOn}</td>
                  <td>{order.status}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </>
    ) : (
      <Navigate to="/shop" />
    )
  );
}

import AppNavbar from '../components/AppNavbar';
import ProductBackground from '../components/background/ProductBackground';
import { useState, useEffect, fragment } from 'react';
import { useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { ShoppingBagOutlined } from '@mui/icons-material';
import Swal from 'sweetalert2';

export default function ProductView() {
  const { productId } = useParams();

  const [ productDetail, setProductDetail  ] = useState(null);

  const fetchProductData = async () => {
    // Get the specific product details from the server using the productId
    let res = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
    const data = await res.json();
    console.log(data);
    setProductDetail(
      <>
        <div className="col-md-8 product-detail">
          <h4>Product Name: {data.name}</h4>
          <p>Product Description: {data.description}</p>
          <p>Stocks:  {data.stocks}</p>
          <div>
            <button className="order-btn" onClick={placeOrder}>
              Order
            </button>
          </div>
          <div className="my-3">
            <button className="order-btn" onClick={addToCart}>
              Add to Cart<ShoppingBagOutlined />
            </button>
          </div>
        </div>
        <div className="col-md-2 product-price">
          ₱ {data.price}
        </div>
      </>
    );
    // Update the product state with the fetched data
  };

const placeOrder = () => {
  // Send a request to place an order for the specified productId
  fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({ productId: productId }) // Use productId directly from the outer scope
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // Check the response to determine if the order was placed successfully
      if (data) {
        // Order placed successfully
        Swal.fire({
          icon: 'success',
          title: 'Order Placed',
          text: 'Your order has been placed successfully.',
        });
        fetchProductData(); // Fetch the updated data after placing the order
      } else {
        // Order placement failed
        Swal.fire({
          icon: 'error',
          title: 'Order Failed',
          text: 'Failed to place your order.',
        });
      }
    });
};


const addToCart = () => {
  // Send a request to add the product to the cart
  fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
    method: 'POST',
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`,
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ productId: productId }) // Use productId directly from the outer scope
  })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // Check the response to determine if the product was added to the cart successfully
      if (data) {
        // Product added to the cart successfully
        Swal.fire({
          icon: 'success',
          title: 'Product Added to Cart',
          text: 'The product has been added to your cart.',
        });
      } else {
        // Failed to add the product to the cart
        Swal.fire({
          icon: 'error',
          title: 'Failed to Add Product to Cart',
          text: 'Failed to add the product to your cart.',
        });
      }
    });
};


  // Fetch the product details on component mount
  useEffect(() => {
    fetchProductData();
  }, []);

  return (
    <div>
      <AppNavbar />
      <ProductBackground />
      <Container>
        <div className="row product">
          <div className="col-md-2">
            <img
              src={process.env.PUBLIC_URL + '/images/sample.jpg'}
              alt="Sample"
              height="150"
            />
          </div>
          {productDetail}
        </div>
      </Container>
    </div>
  );
}